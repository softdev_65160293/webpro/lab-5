import type { Member } from "./Member";
import type { ReceiptItem } from "./ReceiptItem";
import type { User } from "./User";

const defaultReceipt = {
    id: -1,
    createDate: new Date(),
    total: 0,
    receivedAmount: 0,
    change: 0,
    paymentType: 'cash',
    userId: 1,
    user: null,
    memberId: -1 
  }

type Receipt = {
    id: number;
    createDate: Date;
    totalBefore: number;
    memberDiscount: number;
    total: number;
    receivedAmount: number;
    change: number;
    paymentType: string;
    userId: number;
    user?: User;
    memberId: number;
    member?: Member;
    receiptItems?: ReceiptItem[]
}

  export {type Receipt}